package com.cortez.samples.javaee7angular.rest;

import javax.ejb.EJB;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.ArchivePaths;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.cortez.samples.javaee7angular.data.Person;

@RunWith(Arquillian.class)
public class PersonResourceTest2{

	@Deployment
	   public static JavaArchive createTestArchive() {

	      return ShrinkWrap.create(JavaArchive.class)
	         .addClasses(Person.class, PersonResource.class)
	         .addAsManifestResource(
	            "META-INF/test-persistence.xml", 
 	            ArchivePaths.create("persistence.xml"));
	   }

	   @EJB
	   private PersonResource personResource;


	   @Test
	   public void testCanPersistPerson() {

	      Person testPerson = new Person();
	      Person returnedPerson;
	      testPerson.setName("Franta");
	      testPerson.setDescription("Z velkej dialky celeho sveta");
	      testPerson.setImageUrl("http://megaicons.net/static/img/icons_sizes/10/248/512/brown-man-icon.png");
	      
	      personResource.savePerson(testPerson);

	      returnedPerson = personResource.getPerson((long) 0);
	      Assert.assertNotNull(returnedPerson);
	      Assert.assertTrue(returnedPerson.getName() == testPerson.getName());
	      Assert.assertTrue(returnedPerson.getImageUrl() == testPerson.getImageUrl());
	      Assert.assertTrue(returnedPerson.getDescription() == testPerson.getDescription()); 

	   }

	}
