import static com.codeborne.selenide.Selenide.*;
import static com.codeborne.selenide.Condition.*;

import org.junit.Test;
import org.openqa.selenium.By;

public class UITests{
@Test
public void addPersonTest() {
	
	open("http://localhost:8080/javaee7-angular-master/");
	$(By.name("name")).setValue("Saitama");
	$(By.name("description")).setValue("Z-City");
	$(By.name("imageUrl")).setValue("http://vignette3.wikia.nocookie.net/onepunchman/images/2/27/Saitama.png/revision/latest?cb=20130205162513").pressEnter();;
	$(By.className("alert")).shouldHave(text("Record saved successfully!"));
}@Test
public void shortNameTest(){
	open("http://localhost:8080/javaee7-angular-master/");
	$(By.name("name")).setValue("N");
	$(By.cssSelector("div.form-group:nth-child(1) > p:nth-child(5)")).should(appear);
}@Test
public void rightNameTest(){
	open("http://localhost:8080/javaee7-angular-master/");
	$(By.name("name")).setValue("Neo");
	$(By.cssSelector("span.glyphicon:nth-child(2)")).should(appear);
}@Test
public void shortDescTest(){
	open("http://localhost:8080/javaee7-angular-master/");
	$(By.name("description")).setValue("Zion");
	$(By.cssSelector("div.form-group:nth-child(2) > p:nth-child(5)")).should(appear);
}@Test
public void rightDescTest(){
	open("http://localhost:8080/javaee7-angular-master/");
	$(By.name("description")).setValue("City of Zion");
	$(By.cssSelector("span.glyphicon:nth-child(2)")).should(appear);
}@Test
public void wrongUrlTest(){
	open("http://localhost:8080/javaee7-angular-master/");
	$(By.name("imageUrl")).setValue("google");
	$(By.cssSelector("div.form-group:nth-child(3) > p:nth-child(5)")).should(appear);
}@Test
public void rightUrlTest(){
	open("http://localhost:8080/javaee7-angular-master/");
	$(By.name("imageUrl")).setValue("http://vignette3.wikia.nocookie.net/matrix/images/e/ec/Matrix-neo-wallpaper-660x320.jpg/revision/latest?cb=20131002032555");
	$(By.cssSelector("div.form-group:nth-child(3) > span:nth-child(2)")).should(appear);
}@Test
public void selectPersonTest(){
	open("http://localhost:8080/javaee7-angular-master/");
	$(By.cssSelector("div.col1:nth-child(1)")).click();
	$(By.cssSelector(".form > div:nth-child(1)")).should(appear).shouldHave(text("Edit Person"));
}@Test
public void deletePersonTest(){
	open("http://localhost:8080/javaee7-angular-master/");
	$(By.cssSelector("div.ngCell:nth-child(4)")).click();
	$(By.className("alert")).shouldHave(text("Record deleted successfully!"));
}@Test
public void editNameTest(){
	open("http://localhost:8080/javaee7-angular-master/");
	$(By.cssSelector("div.col1:nth-child(1)")).click();
	$(By.name("name")).setValue("Neo").pressEnter();
	$(By.className("alert")).shouldHave(text("Record saved successfully!"));
	$(By.cssSelector("div.col1:nth-child(1)")).shouldHave(text("Neo"));
}@Test
public void editDescTest(){
	open("http://localhost:8080/javaee7-angular-master/");
	$(By.cssSelector("div.col1:nth-child(1)")).click();
	$(By.name("description")).setValue("City of Zion").pressEnter();
	$(By.className("alert")).shouldHave(text("Record saved successfully!"));
}@Test
public void editUrlTest(){
	open("http://localhost:8080/javaee7-angular-master/");
	$(By.cssSelector("div.col1:nth-child(1)")).click();
	$(By.name("imageUrl")).setValue("http://vignette3.wikia.nocookie.net/matrix/images/e/ec/Matrix-neo-wallpaper-660x320.jpg/revision/latest?cb=20131002032555").pressEnter();
	$(By.className("alert")).shouldHave(text("Record saved successfully!"));}
}